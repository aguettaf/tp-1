
public class TP_1_exercice3 {
	
	
	public static void main(String[] args) {
		System.out.println(sumSquaredFor (2));
		sumSquaredWhile (2);
		
	}
	//for function return
	static int sumSquaredFor (int n) {
		int sum=0;
		for (int i=0;i<n+1;i++) {
			sum+=i*i;
		}
		return sum;
		
	}
	
	//while function void
	static void sumSquaredWhile (int n) {
		int sum=0;
		int i=0;
		while (i<n+1) {
			sum+=i*i;
			i++;
		}
		System.out.println(sum);
	}
}

